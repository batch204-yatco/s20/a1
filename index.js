// console.log("Hello World");
// 1. In the s20 folder, create an a1 folder and an index.html and index.js file inside of it.
// 2. Link the index.js file to the index.html file.
// 3. Create a variable number that will store the value of the number provided by the user via the prompt.

let number = Number(prompt("Input number: "));
console.log("The number you provided is: "+number);

// 4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

// 5. Create a condition that if the current value is less than or equal to 50, stop the loop.

// 6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

// 7. Create another condition that if the current value is divisible by 5, print the number.

for (let newnum = number; newnum >= 0; newnum--){

	if (newnum <= 50){
		break;
	}

	if (newnum % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;

	} else if(newnum % 5 === 0){
		console.log(newnum);
	}

}

// 8. Create a variable that will contain the string supercalifragilisticexpialidocious.
// 9. Create another variable that will store the consonants from the string.
// 10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
// 11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
// 12. Create an else statement that will add the letter to the second variable.
let longWord = "supercalifragilisticexpialidocious";
console.log(longWord);
let consonant = "";
for(let i = 0; i < longWord.length; i++){


	if(
		longWord[i] === "a" ||
		longWord[i] === "e" ||
		longWord[i] === "i" ||
		longWord[i] === "o" ||
		longWord[i] === "u" 
	){
		continue;
	} else {
		consonant += longWord[i];
	}
}
console.log(consonant);


// 13. Create a git repository named s20.
// 14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 15. Add the link in Boodle.
